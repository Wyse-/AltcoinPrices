# Altcoin Prices

**This is fork with CoinMarketCap support. You can add CoinMarketCap coins just
like you would add any custom coin (three dots in the upper right -> Manage custom coins).
CoinMarketCap works as a fallback: when you add a custom coin that's not on 
CryptoCompare the app will look for its ticker symbol on CoinMarketCap and fetch
data from there.** 

**Release APK: [Altcoin Prices-fdroid-release.apk](https://gitlab.com/Wyse-/AltcoinPrices/blob/master/Altcoin%20Prices/fdroid/release/Altcoin%20Prices-fdroid-release.apk)**

**MD5:`bb24415d0fad8c793419636a9860c7ff`**

**SHA1:`06706a461d8388ae8af4f26464b6f123d5c2a8cc`**

**SHA256:`7d4930de4b6320eac519de77e91ef0e9d7f4d3081cc062241b4ddde182cb69e4`**

Altcoin Prices is the first and only FLOSS 
(Free Libre Open Source Software) GPLv3 licensed cryptocurrency 
portfolio management app for Android.  
Altcoin Prices does not rely on any central infrastucture, data are 
gathered directly and anonymously from public sources like 
cryptocompare, coinmarketcap, etc.  
Users data never leave the device, the app does not include any tracking 
or malicious library or component, both in its literal sense and as in 
freedom restricting.  
All the most important coins are supported and new ones are added every
week (or even day).  

![screenshots](https://gitlab.com/cfabio/AltcoinPrices/raw/master/screenshots.png)

# FAQ

## How do I install Altcoin Prices?

Altcoin Prices is entirely open source and licensed under GPLv3.
You can check out the source code here on GitLab and use Gradle or 
Android Studio to build your own apk file.

The more convenient way to get Altcoin Prices is from [F-Droid]
(https://f-droid.org/packages/eu.uwot.fabio.altcoinprices/).

[<img src="https://gitlab.com/cfabio/AltcoinPrices/raw/master/f-droid-badge.png"
      alt="Get it on F-Droid"
      height="80">](https://f-droid.org/app/eu.uwot.fabio.altcoinprices)

## How can I support the developer?

I accept cryptocurrencies donations:

### Bitcoin:
<a href="bitcoin:353x3kNMUaAt3i79kQTf3KCJWRAVXSRGpW" target="_blank">bitcoin:353x3kNMUaAt3i79kQTf3KCJWRAVXSRGpW</a>

![bitcoin](https://gitlab.com/cfabio/AltcoinPrices/raw/master/bitcoin-qrcode.png)

### Ethereum (and ERC20 tokens):
<a href="ethereum:0x6e670E49cE8cdBfb65e8903b8CCA2078e73491CF" target="_blank">ethereum:0x6e670E49cE8cdBfb65e8903b8CCA2078e73491CF</a>

![ethereum](https://gitlab.com/cfabio/AltcoinPrices/raw/master/ethereum-qrcode.png)

## Security

Altcoin Prices do not tracks its users in any way, data are fetched 
directly from cryptocompare using its API.
I encourage people to take a look at the code and report any bug or
security hole they can find.

## I found a bug

Please report it using the [issue tracker][issues].
If you are experiencing misbehavior or crash please provide detailed 
steps on how to reproduce the bug. 
Always mention the version of the app you are running. 

[issues]: https://gitlab.com/cfabio/AltcoinPrices/issues
