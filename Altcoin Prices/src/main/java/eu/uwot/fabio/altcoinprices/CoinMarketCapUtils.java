package eu.uwot.fabio.altcoinprices;

import android.util.JsonReader;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;

public class CoinMarketCapUtils {
    public static int getCoinIdFromCoinMarketCap(String coinSymbol) {
        URL url = null;
        HttpURLConnection urlConnection = null;

        try {
            url = new URL("https://api.coinmarketcap.com/v2/listings/");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        int altcoinId = -1;
        try {
            assert url != null;
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setConnectTimeout(5000); // 5 seconds timeout

            InputStream in = urlConnection.getInputStream();
            InputStreamReader isw = new InputStreamReader(in);

            JsonReader jsonReader = new JsonReader(isw);
            String name = "";
            boolean found = false;
            if (jsonReader.hasNext()) {
                jsonReader.beginObject();
                if (jsonReader.nextName().contains("data")) {
                    jsonReader.beginArray();
                    while (jsonReader.hasNext() && !found) {
                        jsonReader.beginObject();
                        while (jsonReader.hasNext() && !found) {
                            name = jsonReader.nextName();
                            if (name.equals("id")) {
                                altcoinId = jsonReader.nextInt();
                            } else if (name.equals("symbol")) {
                                if (jsonReader.nextString().equals(coinSymbol)) {
                                    found = true;
                                }
                            } else if (name.equals("website_slug")) {
                                jsonReader.skipValue();
                            } else if (!name.equals("symbol")) {
                                jsonReader.skipValue();
                            }
                        }
                        if (!found) {
                            jsonReader.endObject();
                        }
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return altcoinId;
    }

    public static float getCoinFiatQuoteCoinMarketCap(int coinId, String currency){
        URL url = null;
        StringBuilder dataSTR = new StringBuilder();

        try {
            url = new URL("https://api.coinmarketcap.com/v2/ticker/" + coinId + "/?convert=" + currency);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        HttpURLConnection urlConnection = null;
        try {
            assert url != null;
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setConnectTimeout(5000); // 5 seconds timeout

            InputStream in = urlConnection.getInputStream();
            InputStreamReader isw = new InputStreamReader(in);

            int data = isw.read();
            while (data != -1) {
                char current = (char) data;
                data = isw.read();
                dataSTR.append(Character.toString(current));
            }
        } catch (SocketTimeoutException e) {
            dataSTR = new StringBuilder("0");
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            assert urlConnection != null;
            urlConnection.disconnect();
        }

        JSONObject json;
        float coinQuote = -1;
        try {
            json = new JSONObject(dataSTR.toString()).getJSONObject("data").getJSONObject("quotes").getJSONObject(currency);
            coinQuote = (float) json.getDouble("price");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return coinQuote;
    }
}
